// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef VOTES_H
#define VOTES_H

#include <QObject>
#include <QAbstractListModel>
#include <QColor>
//структура котика
struct Cat{
    QString id;//image_id
    QString image_url;
    QString color;
    int vote_id;
};

class Votes:public QAbstractListModel
{
    Q_OBJECT

public:
    enum Roles{IdRole=Qt::UserRole + 1,ImageRole,VoteRole,VoteIdRole};
    explicit Votes(QObject* parent = nullptr);
//переопределяем родительские методы
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

public slots:
    void addCatImageList(QString id,QString url);
    void addVote(QString image_id,int vote_id,QString url);
    void postVote(int index,int id);
    void deleteVote(int index);
    void onlyVote();
    void null_size();
    void clearVotesList();
private:
    QVector<Cat> m_data;
    QVector<Cat> list_vote;
};

#endif // VOTES_H
