// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import QtQuick.Controls 2.0
import QtSensors 5.2

Item {
    Column{
        anchors.fill:parent
        spacing:10

        Button{
            text:"to list"
            onClicked:{
                stack.push(list_done_cards_page)
            }
        }


        Button{
            text:"SHAKE"
            onClicked:{
                getTask()
            }
        }

        Text{
            text:window.activity
        }

    }

}
