// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

Window {
    id:window
    width: 640
    height: 480
    visible: true
    title: qsTr("SearchBreeds")
    property var model

    function getINFO(breed_id){
        var request = new XMLHttpRequest()
        request.open('GET',"https://api.thecatapi.com/v1/images/search?breed_ids="+breed_id+"&limit=10")

        request.setRequestHeader('Content-Type', 'application/json')
        request.setRequestHeader('x-api-key', 'live_dNLfNAdpjCCFvSnRTmcvN968fPZrzccwTndgfiqoxoc3EHFyaxjJ2V7pnMheNDHn')

        request.onreadystatechange = function() {
                      if (request.readyState === XMLHttpRequest.DONE) {
                          //200 - код успешного завершения
                          if (request.status && request.status === 200) {
                              window.model = JSON.parse(request.responseText)
                              console.log("RESULT ", request.responseText)


                          } else{
                              console.log("STATUS ",request.status)

                          }
                      }else{
                          console.log("Status Not Done")

                      }
                 }
        request.send()

    }
    ScrollView{

    anchors.fill:parent

    Column{
        anchors.fill:parent
        TextEdit{
            id:input_text
            width:parent.width
            height:50
        }
        Button{
            id:get_data_button
            width:100
            height:50
            text:"GET DATA"

            onClicked: {
                getINFO(input_text.text)
            }
        }

            Repeater{
                model:window.model
                delegate:Image{
                    width:100
                    height:100
                    source:window.model[index].url
                }
            }
       }
    }

}
