// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <edu@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "trash.h"
#include <QDebug>
Trash::Trash(QObject* parent):QObject(parent)
{
    //инициализация случайного элемента
    tmp_index = generator.bounded(0,rubbish.size());
    tmp_rubbish = rubbish[tmp_index];
    m_text = tmp_rubbish.name;
}

QString Trash::text()//геттер текста
{
    return m_text;
}

int Trash::mistakes()//геттер ошибок
{
    return m_mistakes;
}

bool Trash::gameState()//геттер состояния игры
{
    return m_gameState;
}

QString Trash::getMistakeMessage()//геттер поясняющего сообщения
{
    return mistakeMessage;
}



void Trash::restart()//обновляем данные на исходные
{
    rubbish={Rubbish{"сломаный компьютер","special"},Rubbish{"обои","special"},
                                  Rubbish{"испортившейся творог","organic"},Rubbish{"стухшее мясо","organic"},
                                  Rubbish{"книги(не очень хорошие)","paper"},Rubbish{"печатные средства массовой информации","paper"},
                                  Rubbish{"консервная банка","metal"},Rubbish{"металлом","metal"},
                                  Rubbish{"упаковка от шампуня","plastic"},Rubbish{"пластиковые крышки","plastic"}};
    tmp_index = generator.bounded(0,rubbish.size());
    tmp_rubbish = rubbish[tmp_index];
    m_text = tmp_rubbish.name;
    m_mistakes=0;
    m_gameState=false;
    mistakeMessage="";
    emit textChanged();
    emit mistakesChanged();
    emit gameStateChanged();

}

void Trash::setText(QString type)
{

    if(rubbish.size()!=0){//если вектор не пуст
        if(type != tmp_rubbish.type){//если выбранная категария и правильная категория не совпадают фиксиируем ошибки
            mistakeMessage+=tmp_rubbish.name+" ваш ответ: "+type+" правильный ответ: "+tmp_rubbish.type+"\n";//фиксируем текст для вывода ошибок на экране
            m_mistakes+=1;//увеличиваем количество ошибок
            emit mistakesChanged();
        }else{
            rubbish.remove(tmp_index);
            if(!rubbish.size()){
                if(m_mistakes==0){
                    m_text="Хорошая работа!";
                }else{
                    m_text="Твои ошибки = "+QString::number(m_mistakes);
                }
                m_gameState=true;
                emit textChanged();
                emit gameStateChanged();
                return;
            }

        }
        tmp_index = generator.bounded(0,rubbish.size());
        tmp_rubbish = rubbish[tmp_index];
        m_text = tmp_rubbish.name;
        emit textChanged();
    }
    }


