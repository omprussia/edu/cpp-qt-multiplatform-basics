# Задания по теме «Работа с мультимедиа»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Видеопроигрыватель

Работа с сигналами и слотами. Работа с QML для создания простого визуального интерфейса видеопроигрывателя.

### Задача

Реализовать воспроизведение видео. Для выполнения работы требуется видео в формате mp4.

### Решение

Создайте новый проект для Android и Desktop версии. Переключите проект на работу с Desktop.

Перейдите к файлам проекта на вашем компьютере. Создайте новую папку `assets` в той же директории, где лежат `main.cpp` и другие файлы исходного кода. Поместите в папку видео, которое будет использоваться для проигрывания. Вернитесь обратно в Qt Creator. Нажмите правой клавишей мыши на `qml.qrc`. Выберите открытие файла в редакторе. Нажмите **Добавить файлы** и выберите ваш видеофайл. Он должен появиться в списке ресурсов. 

Вернитесь в `main.qml`. Добавьте модуль `QtMultimedia` для отображения видео. Создайте объект класса `Video` для плеера и задайте ему идентификатор, например, `video` для управления воспроизведением. Также нужно задать ему размеры. Ширина будет соответствовать ширине экрана[^1], а высота будет на 100 пикселей меньше для добавления кнопок.

Далее необходимо указать место размещения файла. Обратите внимание на дерево проекта слева. Если вы там не видите папку `assets` с файлом, то нужно пересобрать проект через пункт меню **Сборка** или контекстное меню мыши по имени проекта. В итоге на `main.qml` должна отобразиться папка с вашим видеофайлом. Чтобы не набирать путь к файлу вручную, можно нажать правой клавишей мыши на самом файле и выбрать **Скопировать URL ...**. Вставьте скопированный URL в свойство `source`. Для активации фокуса клавиатуры на плеере задайте свойство `focus` равным `true`. Укажите, будет ли видео отображаться при включении приложения. Для автоматического воспроизведения необходимо установить свойство `autoPlay` равным `true`. Если запустить приложение на устройстве Android, видео будет отображаться. Однако при запуске на Desktop покажет пустой экран. Для Desktop версии необходимо подключить дополнительный модуль в файле `pro`. В строке подключения модулей нужно добавить модуль мультимедиа:

```
QT += quick multimedia
```

Добавьте кнопки управления видео. Для использования класса `Button` импортируйте модуль `QtQuick.Controls`. Создайте ряд и расположите в нем кнопки. Реализуйте управление видео при нажатии на эти кнопки, вызывая соответствующие методы у объекта `video`. Для запуска видео выполните `play()`. Он будет продолжать проигрывание видео с последнего момента. Метод `pause()` остановит воспроизведение. Для перехода в какой-то новый участок видео используется метод `seek(ms)`, где `ms` — миллисекунды с начала файла. Для перезапуска видео достаточно вызвать метод `seek()` без параметров. 

> ![](../../resources/videoplayer_buttons_func.png)

### Адаптация под Android

Для запуска на Android необходимо заменить основной объект на `ApplicationWindow` и удалить размеры окна. Запустите программу на устройстве.

> ![](../../resources/videoplayer_android.png)

### Адаптация под ОС Аврора

При создании проекта необходимо указать разрешение для работы с аудио и видео (**Audio** на экране разрешения для API и **Videos** на экране разрешения для директорий). Также в файл `.spec` необходимо добавить:

*	`Requires: qt5-qtdeclarative-import-multimedia`;
*	`Requires: qt5-qtmultimedia-plugin-mediaservice-gstmediaplayer`.

Более подробно про оформление файлов `.spec` можно посмотреть в [документации](https://developer.auroraos.ru/doc/software_development/guidelines/spec_requirements).

[^1]: ширину и высоту экрана можно получить из свойств `width` и `height` объекта `parent`.

## Задание. Аудиопроигрыватель

Работа с сигналами и слотами. Работа с QML для создания простого визуального интерфейса. Проигрыватель музыки.

### Задача

Реализовать управление треком. Для выполнения работы требуется мелодия в формате mp3.

### Решение

Создайте новый проект для Android и Desktop версии. Переключите проект на работу с Desktop. Перейдите к файлам проекта на компьютере, создайте внутри папку assets и сохраните в нее любой mp3-файл. Несмотря на то, что файл физически добавлен в папку проекта, он не подключен к нему. Для того, чтобы добавить файл из ресурса, необходимо воспользоваться `qml.qrc`. Отредактируйте его по аналогии с прошлым уроком.

Перейдите в файл **main.qml**. Подключите модуль `QtMultimedia`. Создайте внутри объект c `id` `player` класса `Audio`. Класс `Audio` предоставляет возможность автоматической загрузки трека (свойство `autoLoad`) и автоматического начала воспроизведения (`autoPlay`). Установим эти свойства в занчение `true`. По аналогии с `Video` нужно указать источник воспроизведения — `source`. Скопируйте путь к файлу (URL) из дерева проектов и вставьте его в качестве значения этого свойства.

Запустите приложение. Сразу же появится звук мелодии. Для управления (включение и выключение) мелодией можно воспользоваться встроенными методами `play`, `pause`. Создайте простую разметку, состоящую из двух кнопок `play` и `pause`. Для использования кнопок требуется подключить модуль `QtQuick.Controls`. Чтобы происходила реакция на нажатие, добавьте обработку события `onClicked`, где выполните вызов соответствующих методов `player`. Запустите приложение. Нажатие на кнопки должно влиять на воспроизведение.

### Адаптация под Android

Смените устройство для запуска. Внесите изменения в код, как в прошлых уроках — измените основной объект и удалите размеры окна. Запустите приложение на устройстве.

### Адаптация под Aurora

При создании проекта необходимо указать разрешение для работы с музыкой (**Audio** на экране разрешения для API и **Music** на экране разрешения для директорий). Также в файл `.spec` необходимо добавить:

*	`Requires: qt5-qtdeclarative-import-multimedia`;
*	`Requires: qt5-qtmultimedia-plugin-mediaservice-gstmediaplayer`.

Более подробно про оформление файлов `.spec` можно посмотреть в [документации](https://developer.auroraos.ru/doc/software_development/guidelines/spec_requirements).
