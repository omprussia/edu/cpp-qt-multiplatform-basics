# Задания по теме «Введение в разработку на Qt»

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Задание. Знакомство с Qt Creator

Откройте Qt Creator:

> ![](../../resources/qtcreator_start.png)

Сначала открывается раздел домашней страницы **Проекты**, здесь отображаются созданные проекты и путь к ним. Проекты можно удалять из быстрого доступа.
В разделе **Примеры** можно найти приложения и игры как для настольного компьютера, так и для телефона.
Раздел **Учебники** содержит список туториалов и учебников.
Раздел **Магазин** имеет ссылки на дополнительные модули, которые можно подключить в Qt Creator. Ссылки переносят на страницу документации Qt.
Выберите **Create Project...**, в появившемся окне выбора шаблона выберите **Qt Quick Application**. Более подробно виды шаблонов будут рассмотрены в следующих темах.

> ![](../../resources/qtcreator_template.png)

Необходимо задать имя проекта и выбрать папку для его размещения. В названии проекта нельзя использовать служебные имена.

> ![](../../resources/qtcreator_new_project.png)

Существуют различные системы сборки, но в рамках курса предлагается использовать систему сборки **qmake**:

> ![](../../resources/qtcreator_build_tool.png)

Далее необходимо решить, будет ли в дальнейшем в проекте использоваться система автоматического перевода интерфейса **Qt Linguist**. На первых шагах обучения рекомендуется отказаться от этой функции.

> ![](../../resources/qtcreator_translation.png)

Страница выбора комплекта предлагает выбрать платформы для запуска проекта. Ваш набор комплектов и версия Qt для сборки может отличаться. В любом случае, для первого задания выбираем одну систему сборки x86 и одну Android.

> ![](../../resources/qtcreator_build_kit.png)

В последнем окне необходимо выбрать, добавлять ли проект под систему контроля версий. Система контроля версий позволяет получить доступ к коду из различных мест, реализовать совместную разработку кода. В нашем случае рекомендуется пропустить этот шаг, оставив все поля по умолчанию, и нажать кнопку **Завершить**.

> ![](../../resources/qtcreator_cvs.png)

Окно внизу проекта указывает на процесс загрузки. Дождитесь ее окончания. Все процессы загрузки должны закончиться. Отобразится структура проекта:

> ![](../../resources/qtcreator_project_structure.png)

`Window` — базовый объект, который описывает экран. Для мобильных платформ вместо него обычно используется `ApplicationWindow` (требует подключения библиотеки контроллеров).

В примере видны первые свойства объекта:

*	`width` — ширина окна;
*	`height` — высота окна;
*	`visible` — видимость окна (обязательное свойство);
*	`title` — заголовок.

В папке **исходники** содержится класс `main.cpp`:

> ![](../../resources/qtcreator_main_cpp.png)

Здесь содержится основная функция `main`, без которой невозможна компиляция программы. В качестве параметров передаются `argc` и `argv`. `argc` содержит количество параметров, передаваемых в `main()`, `argv[]` — массив указателей на строки. `argc` всегда больше нуля, так как первым параметром передается имя исполняемого файла.

Выше находится файл `sample.pro` (имя файла совпадает с названием проекта). В этом файле происходит подключение дополнительных библиотек. Например, для работы с мультимедиа или с датчиками. Также здесь указываются добавленные в проект классы. Все С++ и QML файлы должны быть добавлены в файл `*.pro`. Это происходит автоматически, если создавать новый класс через среду разработки.

> ![](../../resources/qtcreator_pro_content.png)

Откройте снова файл `main.qml`. Здесь отображается ошибка. Обратите внимание на устройство для запуска:

> ![](../../resources/qtcreator_launch_device.png)

При добавлении комплекта для сборки под Android эта платформа выбирается по умолчанию для запуска, поэтому для запуска на компьютере нужно сменить комплект для сборки. Для того, чтобы сменить комплект, нажмите на эту кнопку и выберите версию Desktop. Для тестирования приложения лучше использовать режим сборки **Отладка**.

При нажатии на кнопку в виде зелёной стрелки происходит запуск приложения. Проверьте работу приложения. Версия x86 не требует включенных эмуляторов. Внизу появятся сообщения приложения:

> ![](../../resources/qtcreator_helloworld.png)

Это меню вывода позволяет определить проблемы у сборки. Его удобно использовать для отладки, однако могут отображаться не все ошибки.

Мы изучили базовые элементы среды разработки Qt Creator. Более подробно интерфейс будет рассматриваться далее по мере необходимости.

## Задание. Знакомство с SDK Aurora

Если вы используете эмулятор для запуска приложения, можете активировать его заранее. Для этого запустите **Oracle VM Virtual Box**. Для работы эмулятора необходимо сначала подключить виртуальную машину с Aurora OS (Aurora Build Engine). После запуска машины (когда отобразится логотип Aurora) запустите эмулятор. Посмотреть, как запускать эмулятор, можно [в документации](https://developer.auroraos.ru/doc/software_development/sdk/run_and_debug). 

> ![](../../resources/aurora_emu_screenshot.png)

Эмулятор практически полностью воспроизводит возможности операционной системы Аврора. Откройте SDK Aurora. В разделе **Примеры** содержатся рабочие примеры приложений.

> ![](../../resources/aurora_start_screen.png)

В разделе **Учебники** вы можете найти обучающие материалы. Вернитесь к разделу **Проекты**. Здесь находится кнопка **Создать**. Выберите строку **Приложение Qt Quick для ОС Аврора**:

> ![](../../resources/aurora_template.png)

Как видно из названия, она будет использовать QtQuick для создания интерфейса. В дальнейшем вы рассмотрите его подробнее.

Необходимо задать имя проекта и выбрать папку для его размещения. В названии проекта нельзя использовать служебные имена. Имя проекта должно быть информативным (отражать содержимое). Подробнее можно изучить вопрос по [ссылке](https://developer.auroraos.ru/doc/software_development/guidelines/rpm_requirements).

> ![](../../resources/aurora_new_project.png)

На следующем экране добавляется описание проекта. Здесь можно ввести информацию на русском и английском языках (в соответствии с шаблоном). 

> ![](../../resources/aurora_project_description.png)

Далее необходимо настроить разрешения, которые приложение будет запрашивать на устройстве/эмуляторе. [Подробнее](https://developer.auroraos.ru/doc/software_development/reference/user_data). По умолчанию в операционной системе доступ ко всем дополнительным модулям запрещен. Его необходимо разрешить. Например, приложение, которое требует доступа в интернет, но которому не задано в настройках проекта получение этого разрешения, не будет корректно работать. Сначала настраиваются разрешения для API - устройства и компоненты ОС, с которыми сможет взаимодействовать приложение.

> ![](../../resources/aurora_permissions_API.png)

Затем необходимо указать директории, к которым будет разрешен доступ приложению.

> ![](../../resources/aurora_permissions_dirs.png)

Существуют различные системы сборки, но в рамках курса предлагается использовать систему сборки **qmake**:

> ![](../../resources/aurora_build_tool.png)

На следующем экране отображаются комплекты сборки. [Подробнее](https://developer.auroraos.ru/doc/software_development/sdk/run_and_debug). **i486** соответствует эмулятору, а **atmv7hl** — физическому устройству. В примере рассматривается запуск на эмуляторе:

> ![](../../resources/aurora_build_kit.png)

В последнем окне необходимо выбрать, добавлять ли проект под систему контроля версий. Система контроля версий позволяет получить доступ к коду из различных мест, реализовать совместную разработку кода. В нашем случае предлагается пропустить этот шаг, оставив все поля по умолчанию и нажать кнопку **Завершить**.

> ![](../../resources/aurora_cvs.png)

После нажатия на кнопку «Завершить» проект будет создан. Обратите внимание на его структуру. Она будет немного изменена по сравнению с классическим Qt Creator:

> ![](../../resources/aurora_project_structure.png)

Папка «Исходники» содержит исходный код на С++. Весь код на QML содержится в папке «QML». Также существует папка «Другие файлы» для добавления дополнительных объектов. Здесь вы можете найти изображения иконок приложения.
Главным файлом QML является тот файл, который находится на верхнем уровне. Его имя соответствует имени проекта.

> ![](../../resources/aurora_sample_qml.png)

В проекте имеются три дополнительных файла QML. Они добавляются автоматически в качестве примера. Их можно заменить, но не забудьте отразить эти изменения в главном файле QML, чтобы в нем не было обращения к несуществующим файлам. Запустите проект:

> ![](../../resources/aurora_example_test.png)

На экране должно появиться окно эмулятора с запущенным приложением **Template**, которое имеет только одну кнопку **i**, нажатие на которую отображает информацию об этом приложении.
