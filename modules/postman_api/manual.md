# Postman API

Copyright&nbsp;©&nbsp;2022–2023 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

**Postman** — это платформа API, позволяющая разработчикам проектировать, создавать, тестировать и проверять свои API. Не всегда документация предоставляет полную информацию о запросах. В таких случаях поможет Postman.

Рассмотрите [пример](https://jsonplaceholder.typicode.com/):

>![](../../resources/postman_example_preview.png) 

Непонятно, что вернет результат запроса. Есть только сами запросы. В данном случае запрос можно посмотреть на самом сайте, нажав на ссылку, но это доступно не всегда. Иногда информация в документации может не совпадать с реальным ответом сервера, и тогда возникнут проблемы.

Перед тем, как перейти к рассмотрению запросов, необходимо определиться с основными понятиями. И начать стоит с ответов сервера. Для интерпретирования состояния HTTP-запроса последний возвращает код. Коды (согласно документации MDV Web Docs):

1.	информационные: 100—199.
2.	успешные: 200—299.
3.	перенаправления: 300—399.
4.	клиентские ошибки: 400—499.
5.	серверные ошибки: 500—599.

Для тестирования запросов необходимо их запомнить или же записать, чтобы всегда иметь возможность интерпретировать результат и не перепутать успешный ответ с ошибкой.
Некоторые API, например, те, которые регистрируют пользователя, используют токены. **Api-token** — это уникальный ключ для входа в личный кабинет. Он генерируется системой. Чаще всего записывается в заголовке запроса.

Перейдем к самим запросам. HTTP-запросы:

*	GET — запрос о предоставлении ресурса (извлекает данные);
*	HEAD — запрос о предоставление ресурса, но без тела ответа;
*	POST — запрос об отправке сущности к ресурсу;
*	PUT — запрос о замене представления ресурса;
*	DELETE — запрос об удалении;
*	CONNECT — запрос об установке «туннеля» к серверу;
*	OPTIONS — описание параметров соединения с ресурсом;
*	TRACE — запрос о вызове возвращаемого текстового сообщения с ресурса;
*	PATCH — запрос о частичном изменении ресурса.

В API используются следующие запросы:

*	GET;
*	POST;
*	PUT / PATCH;
*	DELETE;
*	OPTIONS.

Далее будут рассмотрены первые четыре вида запроса.
