#  Модули для курсов по&nbsp;основам мультиплатформенной разработки на&nbsp;C++ и&nbsp;Qt

Copyright&nbsp;©&nbsp;2022‒2024 ООО&nbsp;«Открытая мобильная платформа».
Этот документ предоставляется в&nbsp;соответствии
с&nbsp;[Публичной лицензией Creative Commons с&nbsp;указанием авторства версии&nbsp;4.0 Международная](../../LICENSE.CC-BY-4.0.ru.md).

## Введение в&nbsp;Qt

*	[Введение в&nbsp;разработку на&nbsp;Qt](./devel_tools)
    *	[Руководство](./devel_tools/manual.md)
    *	[Презентация](./devel_tools/lecture.odp)
    *	[Задание](./devel_tools/tasks.md)
*	[Введение в&nbsp;Qt&nbsp;Quick](./qml_intro)
    *	[Руководство](./qml_intro/manual.md)
    *	[Презентация](./qml_intro/lecture.odp)
    *	[Задание](./qml_intro/tasks.md)
*	[Знакомство с&nbsp;Qt&nbsp;Widgets](./qtwidgets)
    *	[Руководство](./qtwidgets/manual.md)
    *	[Презентация](./qtwidgets/lecture.odp)
    *	[Задание](./qtwidgets/tasks.md)
*	[Сигналы и&nbsp;слоты](./c_qml_integration)
    *	[Руководство](./c_qml_integration/manual.md)
    *	[Презентация](./c_qml_integration/lecture.odp)
    *	[Задание](./c_qml_integration/tasks.md)

## Взаимодействие с&nbsp;объектами

*	[Знакомство с&nbsp;MouseArea](./mousearea_signals)
    *	[Руководство](./mousearea_signals/manual.md)
    *	[Презентация](./mousearea_signals/lecture.odp)
    *	[Задание](./mousearea_signals/tasks.md)
    *	[Решение задания](../projects/calculator_qtcreator)
*	[Таймер](./timer/)
    *	[Руководство](./timer/manual.md)
    *	[Задание](./timer/tasks.md)

## Работа с&nbsp;данными и&nbsp;файлами

*	[Работа с&nbsp;мультимедиа](./multimedia_player)
    *	[Задание](./multimedia_player/tasks.md)
*	[Ввод и&nbsp;валидация текста](./validator/)
    *	[Руководство](./validator/manual.md)
    *	[Презентация](./validator/lecture.odp)
    *	[Задание](./validator/tasks.md)
*	[Списки](./lists)
    *	[Руководство](./lists/manual.md)
    *	[Задание](./lists/tasks.md)
    *	[Решение задания](../projects/trash2_qtcreator)
*	[Архитектура приложения](./application_architecture/)
    *	[Руководство](./application_architecture/manual.md)
    *	[Задание](./application_architecture/tasks.md)
    *	[Решение задания](../projects/running_game_qtcreator)
*	[Работа с&nbsp;датчиками](./task_sensors)
    *	[Руководство](./task_sensors/manual.md)
    *	[Задание](./task_sensors/tasks.md)
    *	[Решение задания Фанты](../projects/fants_qtcreator)

## Клиент-серверное взаимодействие и&nbsp;API

*	[Клиент-серверное взаимодействие в&nbsp;Qt](./client-server)
    *	[Руководство](./client-server/manual.md)
    *	[Презентация](./client-server/lecture.odp)
    *	[Вопросы](./client-server/tests.md)
*	[Работа с&nbsp;JSON](./json)
    *	[Руководство](./json/manual.md)
    *	[Задание](./json/tasks.md)
*	[Postman API](./postman_api)
    *	[Руководство](./postman_api/manual.md)
    *	[Презентация](./postman_api/lecture.odp)
    *	[Задание](./postman_api/tasks.md)
*	[Jikan API](./jikan_api)
    *	[Руководство](./jikan_api/manual.md)
    *	[Задание](./jikan_api/tasks.md)
    *	[Решение задания Rick and Morty API](../projects/rick_and_morty_qtcreator)
*	[Cat API](./cat_api_fav)
    *	[Руководство](./cat_api_fav/manual.md)
    *	[Задание](./cat_api_fav/tasks.md)
    *	[Решение задания Cat API GET](../projects/cat_api_search_qtcreator)
    *	[Решение задания Cat API Votes](../projects/api_votes_qtcreator)
    *	[Решение задания Weather API](../projects/weather_qtcreator)
